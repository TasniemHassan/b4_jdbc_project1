package stepDef.databaseStepDef;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import utils.DBUtil;

import java.util.ArrayList;
import java.util.List;

public class databaseStepDef {
    public List<List<Object>> actualQuery = new ArrayList<>();

    @Given("User is able to connect to database")
    public void user_is_able_to_connect_to_database() {
        DBUtil.createDBConnection();
    }

    @When("User send the {string} to database and gets list")
    public void userSendTheToDatabaseAndGetsList(String query) {
        actualQuery=DBUtil.getQueryResultList(query);
    }

    @Then("Validate the employees first name and last name who reports to Payam")
    public void validateTheEmployeesFirstNameAndLastNameWhoReportsToPayam(DataTable names) {
        List<List<String>> listFromFeature = names.asLists();
        for (int i = 0; i < listFromFeature.size(); i++) {
            Assert.assertEquals(listFromFeature.get(i), listFromFeature.get(i));
        }
    }
}
