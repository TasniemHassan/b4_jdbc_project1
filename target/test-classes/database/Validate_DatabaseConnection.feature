Feature: As a QE, I validate that I can connect to DB
  @db
  Scenario Outline: Validating the Database Connection
    Given User is able to connect to database
    When User send the "<query>" to database and gets list
    Then Validate the employees first name and last name who reports to Payam
      | Neena   | Kochhar   |
      | Lex     | De Haan   |
      | Den     | Raphaely  |
      | Matthew | Weiss     |
      | Adam    | Fripp     |
      | Payam   | Kaufling  |
      | Shanta  | Vollman   |
      | Kevin   | Mourgos   |
      | John    | Russell   |
      | Karen   | Partners  |
      | Alberto | Errazuriz |
      | Gerald  | Cambrault |
      | Eleni   | Zlotkey   |
      | Michael | Hartstein |
    Examples: Database query
      |query  |
      |select first_name, last_name from employees where manager_id = (select manager_id from employees where first_name='Payam') |
